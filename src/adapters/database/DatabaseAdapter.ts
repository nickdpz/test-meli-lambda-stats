import { ResponseDatabaseModel } from '../../models/ResponseDatabaseModel';

export interface DatabaseAdapter {
    get(): Promise<ResponseDatabaseModel>;
}
