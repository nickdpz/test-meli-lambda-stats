/* eslint-disable @typescript-eslint/no-explicit-any */
import 'reflect-metadata';
import { DatabaseAdapter } from '../DatabaseAdapter';
import { CONSTANTS, TYPES } from '../../../utils/Constants';
import { DocumentClient } from 'aws-sdk/clients/dynamodb';
import { inject, injectable } from 'inversify';
import { Logger } from '../../../utils/logger/Logger';
import { ResponseDatabaseModel } from '../../../models/ResponseDatabaseModel';

@injectable()
export class DynamoDatabaseAdapter implements DatabaseAdapter {
    private documentClient = new DocumentClient();

    constructor(@inject(TYPES.Logger) private LOGGER: Logger) {}

    async get(): Promise<ResponseDatabaseModel> {
        const parameters: DocumentClient.QueryInput = {
            TableName: CONSTANTS.TABLE_NAME,
            ExpressionAttributeValues: {
                ':dt': CONSTANTS.DATA_TYPE,
                ':dte': CONSTANTS.DATE,
            },
            KeyConditionExpression: '#data_type = :dt and #date = :dte',
            ExpressionAttributeNames: {
                '#data_type': 'DATA_TYPE',
                '#date': 'DATE',
            },
        };
        this.LOGGER.info('Query dynamo', parameters);
        const result = await this.documentClient.query(parameters).promise();
        console.log('Rsult', result);
        return {
            count_mutant_dna: result.Items[0].COUNT_MUTANT_DNA,
            count_human_dna: result.Items[0].COUNT_HUMAN_DNA,
        };
    }
}
