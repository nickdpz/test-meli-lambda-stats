import { Container } from 'inversify';
import { TYPES } from '../utils/Constants';
import { MainPresenter } from '../presenters/MainPresenter';
import { ApiGWPresenter } from '../presenters/apigw/ApiGWPresenter';
import { MainService } from '../services/MainService';
import { MainServiceImpl } from '../services/MainServiceImpl';
import { DatabaseAdapter } from '../adapters/database/DatabaseAdapter';
import { DynamoDatabaseAdapter } from '../adapters/database/dynamo/DynamoDatabaseAdapter';
import { MainController } from '../controllers/MainController';
import { ApiGWController } from '../controllers/apigw/ApiGWController';
import { WinstonLogger } from '../utils/logger/winston/WinstonLogger';
import { Logger } from '../utils/logger/Logger';

const AppContainer: Container = new Container();

AppContainer.bind<MainPresenter>(TYPES.MainPresenter).to(ApiGWPresenter);
AppContainer.bind<MainService>(TYPES.MainService).to(MainServiceImpl);
AppContainer.bind<MainController>(TYPES.MainController).to(ApiGWController);
AppContainer.bind<DatabaseAdapter>(TYPES.DatabaseAdapter).to(DynamoDatabaseAdapter);
AppContainer.bind<Logger>(TYPES.Logger).to(WinstonLogger);

export { AppContainer };
