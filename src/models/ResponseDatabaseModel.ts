export interface ResponseDatabaseModel {
    count_mutant_dna: number;
    count_human_dna: number;
}
