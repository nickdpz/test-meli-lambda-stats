/* eslint-disable @typescript-eslint/no-explicit-any */
import 'reflect-metadata';
import { inject, injectable } from 'inversify';
import { TYPES } from '../utils/Constants';
import { Logger } from '../utils/logger/Logger';
import { MainPresenter } from '../presenters/MainPresenter';
import { MainService } from './MainService';
import { DatabaseAdapter } from '../adapters/database/DatabaseAdapter';
import { ResponseServiceModel } from '../models/ResponseServiceModel';

@injectable()
export class MainServiceImpl implements MainService {
    constructor(
        @inject(TYPES.DatabaseAdapter)
        private databaseProvider: DatabaseAdapter,
        @inject(TYPES.MainPresenter)
        private presenter: MainPresenter,
        @inject(TYPES.Logger) private LOGGER: Logger
    ) {}
    async processData(): Promise<any> {
        try {
            const response = await this.databaseProvider.get();
            const ratio =
                response.count_mutant_dna / (response.count_human_dna + response.count_mutant_dna);
            const result: ResponseServiceModel = {
                ...response,
                ratio,
            };
            this.LOGGER.debug('Result query', result);
            return this.presenter.generateSuccessResponse(result);
        } catch (error) {
            this.LOGGER.error({ error });
            return this.presenter.generateInternalErrorResponse('Internal error, dont get data');
        }
    }
}
