/* eslint-disable @typescript-eslint/no-explicit-any */
import * as AWSMock from 'aws-sdk-mock';
import * as AWS from 'aws-sdk';
import { DynamoDatabaseAdapter } from '../../../src/adapters/database/dynamo/DynamoDatabaseAdapter';
import { DatabaseAdapter } from '../../../src/adapters/database/DatabaseAdapter';
import { WinstonLogger } from '../../../src/utils/logger/winston/WinstonLogger';

const result = {
    Items: [
        {
            COUNT_MUTANT_DNA: 1,
            COUNT_HUMAN_DNA: 1,
        },
    ],
};
describe('DynamoDatabaseAdapter Test Suite', () => {
    it('adapater save successfully', async () => {
        AWSMock.setSDKInstance(AWS);
        AWSMock.mock(
            'DynamoDB.DocumentClient',
            'query',
            (parameters: any, callback: (str: string, result: any) => void) => {
                console.log('Intro mock dynamo success');
                callback(null, result);
            }
        );
        const adapter: DatabaseAdapter = new DynamoDatabaseAdapter(new WinstonLogger());
        await expectAsync(adapter.get()).toBeResolvedTo({
            count_mutant_dna: 1,
            count_human_dna: 1,
        });
        AWSMock.restore('DynamoDB.DocumentClient');
    });
    it('adapater throw error', async () => {
        AWSMock.setSDKInstance(AWS);
        AWSMock.mock(
            'DynamoDB.DocumentClient',
            'query',
            (parameters: any, callback: (str: string) => void) => {
                console.log('Intro mock dynamo error');
                callback('Error');
            }
        );
        const adapter: DatabaseAdapter = new DynamoDatabaseAdapter(new WinstonLogger());
        await expectAsync(adapter.get()).toBeRejected();
        AWSMock.restore('DynamoDB.DocumentClient');
    });
});
